﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintshopViewer.Domain.Dtos
{
    public class Response<T> where T : class
    {
        public bool Success { get; set; }

        public T Result { get; set; }

        public Response(bool success, T result)
        {
            Success = success;
            Result = result;
        }
    }
}
