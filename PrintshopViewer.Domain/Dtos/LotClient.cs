﻿using System;

namespace PrintshopViewer.Domain.Dtos
{
    public class LotClient
    {
        public string CodeClient { get; set; }
    }

    public class LotClientRetour
    {
        public int LotId { get; set; }
        public string FoliodId { get; set; }
        public DateTime? DateCreation { get; set; }
        public int? NombreCourriers { get; set; }
        public DateTime? DateDepot { get; set; }
        public bool? ValidationClient { get; set; }
        public DateTime? DateValidationClient { get; set; }
        public string MotifRefusClient { get; set; }
        public DateTime? DateAbandon { get; set; }
        public string MotifAbandon { get; set; }
        public DateTime? DateConfirmationImpression { get; set; }
        public DateTime? DateExpedition { get; set; }

    }
}
