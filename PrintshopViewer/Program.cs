using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PrintshopViewer.BusinessLayer.Services;
using PrintshopViewer.Domain.Configuration;
using System;
using System.IO;
using System.Windows.Forms;

namespace PrintshopViewer
{
    internal static class Program
    {
        private static IPrintshopService _printshopService;
        private static SSOConfiguration _ssoConfiguration;

        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        public static void Main()
        {
            BuildConfiguration();

            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FrmAccueil(_printshopService, _ssoConfiguration));
        }
        private static void BuildConfiguration()
        {
            var builder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("config.json", optional: false);

            IConfiguration configuration = builder.Build();

            //config json
            PrintshopAPIConfiguration printshopAPIConfiguration = new();
            configuration.GetSection(nameof(PrintshopAPIConfiguration)).Bind(printshopAPIConfiguration);
            SSOConfiguration ssoConfiguration = new();
            configuration.GetSection(nameof(SSOConfiguration)).Bind(ssoConfiguration);

            var services = new ServiceCollection();
            services.AddSingleton<PrintshopAPIConfiguration>(printshopAPIConfiguration);
            services.AddSingleton<SSOConfiguration>(ssoConfiguration);
            services.AddScoped<IPrintshopService, PrintshopService>();
            ServiceProvider serviceProvider = services.BuildServiceProvider();

            _printshopService = serviceProvider.GetService<IPrintshopService>();
            _ssoConfiguration = ssoConfiguration;
        }
    }
}
