﻿using System;
using System.Windows.Forms;

namespace PrintshopViewer
{
    public partial class FrmMotifRefus : Form
    {
        #region Déclarations
        public string MotifRefus
        {
            get
            {
                return txtMotifRefus.Text;
            }
        }

        private const int CP_NOCLOSE_BUTTON = 0x200;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
                return myCp;
            }
        }

        #endregion

        #region Constructeurs
        public FrmMotifRefus(string Message, string titre, string libelleInput)
        {
            InitializeComponent();
            this.Text = titre;
            lblMessage.Text = Message;
            lblTitreInput.Text = libelleInput;
        }

        #endregion

        #region Evénements
        private void btnValider_Click(object sender, EventArgs e)
        {
            Valider();
        }
        private void btnRefuser_Click(object sender, EventArgs e)
        {
            Refuser();
        }
        #endregion

        #region Fonctionnalités
        private void Valider()
        {
            DialogResult = DialogResult.Yes;
        }
        private void Refuser()
        {
            DialogResult = DialogResult.No;
        }

        #endregion
    }
}
