﻿namespace PrintshopViewer
{
    partial class FrmAccueil
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmAccueil));
            this.lblStatutValidationClient = new System.Windows.Forms.Label();
            this.cmbStatutValidationClient = new System.Windows.Forms.ComboBox();
            this.dtgLotsClient = new System.Windows.Forms.DataGridView();
            this.btnValiderLots = new System.Windows.Forms.Button();
            this.btnRefuserLots = new System.Windows.Forms.Button();
            this.pnlAccueil = new System.Windows.Forms.Panel();
            this.lblEtat = new System.Windows.Forms.Label();
            this.backgroundWorker = new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.dtgLotsClient)).BeginInit();
            this.pnlAccueil.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblStatutValidationClient
            // 
            this.lblStatutValidationClient.AutoSize = true;
            this.lblStatutValidationClient.Location = new System.Drawing.Point(15, 16);
            this.lblStatutValidationClient.Name = "lblStatutValidationClient";
            this.lblStatutValidationClient.Size = new System.Drawing.Size(189, 15);
            this.lblStatutValidationClient.TabIndex = 2;
            this.lblStatutValidationClient.Text = "Statut de validation du bon à tirer :";
            this.lblStatutValidationClient.UseWaitCursor = true;
            // 
            // cmbStatutValidationClient
            // 
            this.cmbStatutValidationClient.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.cmbStatutValidationClient.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStatutValidationClient.FormattingEnabled = true;
            this.cmbStatutValidationClient.Location = new System.Drawing.Point(210, 12);
            this.cmbStatutValidationClient.Name = "cmbStatutValidationClient";
            this.cmbStatutValidationClient.Size = new System.Drawing.Size(162, 23);
            this.cmbStatutValidationClient.TabIndex = 3;
            this.cmbStatutValidationClient.UseWaitCursor = true;
            this.cmbStatutValidationClient.SelectionChangeCommitted += new System.EventHandler(this.cmbStatutValidationClient_SelectionChangeCommitted);
            // 
            // dtgLotsClient
            // 
            this.dtgLotsClient.AllowUserToAddRows = false;
            this.dtgLotsClient.AllowUserToDeleteRows = false;
            this.dtgLotsClient.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgLotsClient.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtgLotsClient.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dtgLotsClient.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgLotsClient.Location = new System.Drawing.Point(15, 53);
            this.dtgLotsClient.Name = "dtgLotsClient";
            this.dtgLotsClient.ReadOnly = true;
            this.dtgLotsClient.RowTemplate.Height = 25;
            this.dtgLotsClient.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgLotsClient.Size = new System.Drawing.Size(682, 368);
            this.dtgLotsClient.TabIndex = 4;
            this.dtgLotsClient.UseWaitCursor = true;
            this.dtgLotsClient.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dtgLotsClient_CellMouseDoubleClick);
            // 
            // btnValiderLots
            // 
            this.btnValiderLots.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnValiderLots.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnValiderLots.Location = new System.Drawing.Point(713, 53);
            this.btnValiderLots.Name = "btnValiderLots";
            this.btnValiderLots.Size = new System.Drawing.Size(75, 23);
            this.btnValiderLots.TabIndex = 5;
            this.btnValiderLots.Text = "Valider";
            this.btnValiderLots.UseVisualStyleBackColor = true;
            this.btnValiderLots.UseWaitCursor = true;
            this.btnValiderLots.Click += new System.EventHandler(this.btnValiderLots_Click);
            // 
            // btnRefuserLots
            // 
            this.btnRefuserLots.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRefuserLots.Location = new System.Drawing.Point(713, 82);
            this.btnRefuserLots.Name = "btnRefuserLots";
            this.btnRefuserLots.Size = new System.Drawing.Size(75, 23);
            this.btnRefuserLots.TabIndex = 6;
            this.btnRefuserLots.Text = "Refuser";
            this.btnRefuserLots.UseVisualStyleBackColor = true;
            this.btnRefuserLots.UseWaitCursor = true;
            this.btnRefuserLots.Click += new System.EventHandler(this.btnRefuserLots_Click);
            // 
            // pnlAccueil
            // 
            this.pnlAccueil.Controls.Add(this.lblEtat);
            this.pnlAccueil.Controls.Add(this.dtgLotsClient);
            this.pnlAccueil.Controls.Add(this.btnRefuserLots);
            this.pnlAccueil.Controls.Add(this.lblStatutValidationClient);
            this.pnlAccueil.Controls.Add(this.btnValiderLots);
            this.pnlAccueil.Controls.Add(this.cmbStatutValidationClient);
            this.pnlAccueil.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlAccueil.Location = new System.Drawing.Point(0, 0);
            this.pnlAccueil.Name = "pnlAccueil";
            this.pnlAccueil.Size = new System.Drawing.Size(800, 450);
            this.pnlAccueil.TabIndex = 7;
            this.pnlAccueil.UseWaitCursor = true;
            // 
            // lblEtat
            // 
            this.lblEtat.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblEtat.AutoSize = true;
            this.lblEtat.Location = new System.Drawing.Point(15, 426);
            this.lblEtat.Name = "lblEtat";
            this.lblEtat.Size = new System.Drawing.Size(16, 15);
            this.lblEtat.TabIndex = 7;
            this.lblEtat.Text = "...";
            this.lblEtat.UseWaitCursor = true;
            // 
            // backgroundWorker
            // 
            this.backgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker_DoWork);
            // 
            // FrmAccueil
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.pnlAccueil);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmAccueil";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Printshop Viewer";
            this.UseWaitCursor = true;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Accueil_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtgLotsClient)).EndInit();
            this.pnlAccueil.ResumeLayout(false);
            this.pnlAccueil.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lblStatutValidationClient;
        private System.Windows.Forms.ComboBox cmbStatutValidationClient;
        private System.Windows.Forms.DataGridView dtgLotsClient;
        private System.Windows.Forms.Button btnValiderLots;
        private System.Windows.Forms.Button btnRefuserLots;
        private System.Windows.Forms.Panel pnlAccueil;
        private System.ComponentModel.BackgroundWorker backgroundWorker;
        private System.Windows.Forms.Label lblEtat;
    }
}
