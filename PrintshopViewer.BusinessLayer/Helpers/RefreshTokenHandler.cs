﻿using IdentityModel.OidcClient;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace PrintshopViewer.BusinessLayer.Helpers
{
    /// <summary>
    /// Event argument with the reafreshed token
    /// </summary>
    public class TokenRefreshedEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TokenRefreshedEventArgs" /> class.
        /// </summary>
        /// <param name="accessToken">The access token.</param>
        public TokenRefreshedEventArgs(string accessToken, string refreshToken, string identityToken)
        {
            AccessToken = accessToken;
            RefreshToken = refreshToken;
            IdentityToken = identityToken;
        }
        /// <summary>
        /// Gets the access token.
        /// </summary>
        /// <value>
        /// The access token.
        /// </value>
        public string AccessToken { get; set; }
        /// <summary>
        /// Gets the refresh token.
        /// </summary>
        /// <value>
        /// The refresh token.
        /// </value>
        public string RefreshToken { get; }
        /// <summary>
        /// Gets the identity token.
        /// </summary>
        public string IdentityToken { get; set; }
    }
    /// <summary>
    /// HTTP message delegating handler that encapsulates token handling and refresh
    /// </summary>
    public class RefreshTokenHandler : HttpClientHandler
    {
        private readonly SemaphoreSlim _lock = new SemaphoreSlim(1, 1);
        private readonly OidcClient _oidcClient;
        private string _accessToken;
        private string _refreshToken;
        private readonly string _identityToken;
        private bool _disposed;
        /// <summary>
        /// Gets or sets the timeout
        /// </summary>
        public TimeSpan Timeout { get; set; } = TimeSpan.FromSeconds(5);
        /// <summary>
        /// Gets the current access token
        /// </summary>
        public string AccessToken
        {
            get
            {
                if (_lock.Wait(Timeout))
                {
                    try
                    {
                        return _accessToken;
                    }
                    finally
                    {
                        _lock.Release();
                    }
                }
                return null;
            }
        }
        /// <summary>
        /// Gets the current refresh token
        /// </summary>
        public string RefreshToken
        {
            get
            {
                if (_lock.Wait(Timeout))
                {
                    try
                    {
                        return _refreshToken;
                    }
                    finally
                    {
                        _lock.Release();
                    }
                }
                return null;
            }
        }
        /// <summary>
        /// Gets the current refresh token
        /// </summary>
        public string IdentityToken
        {
            get
            {
                if (_lock.Wait(Timeout))
                {
                    try
                    {
                        return _identityToken;
                    }
                    finally
                    {
                        _lock.Release();
                    }
                }
                return null;
            }
        }
        /// <summary>
        /// Occurs when the tokens were refreshed successfully
        /// </summary>
        public event EventHandler<TokenRefreshedEventArgs> TokenRefreshed = delegate { };
        /// <summary>
        /// Initializes a new instance of the <see cref="RefreshTokenHandler" /> class.
        /// </summary>
        /// <param name="oidcClient">The oidc client.</param>
        /// <param name="accessToken">The access token.</param>
        /// <param name="refreshToken">The refresh token.</param>
        /// <param name="innerHandler">The inner handler.</param>
        /// <exception cref="ArgumentNullException">oidcClient</exception>
        public RefreshTokenHandler(OidcClient oidcClient, string accessToken, string refreshToken, string identityToken)
        {
            _oidcClient = oidcClient ?? throw new ArgumentNullException(nameof(oidcClient));
            if (String.IsNullOrEmpty(refreshToken)) throw new ArgumentNullException(nameof(refreshToken));
            _refreshToken = refreshToken;
            if (String.IsNullOrEmpty(accessToken)) throw new ArgumentNullException(nameof(accessToken));
            _accessToken = accessToken;
            if (String.IsNullOrEmpty(identityToken)) throw new ArgumentNullException(nameof(identityToken));
            _identityToken = identityToken;
        }
        /// <summary>
        /// Sends an HTTP request to the inner handler to send to the server as an asynchronous operation.
        /// </summary>
        /// <param name="request">The HTTP request message to send to the server.</param>
        /// <param name="cancellationToken">A cancellation token to cancel operation.</param>
        /// <returns>
        /// Returns <see cref="T:System.Threading.Tasks.Task`1" />. The task object representing the asynchronous operation.
        /// </returns>
        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var tokenRequired = !request.RequestUri.AbsoluteUri.Contains("connect/token");
            if (tokenRequired)
            {
                var accessToken = await GetAccessTokenAsync(cancellationToken);
                if (String.IsNullOrEmpty(accessToken))
                {
                    if (await RefreshTokensAsync(cancellationToken) == false)
                    {
                        return new HttpResponseMessage(HttpStatusCode.Unauthorized) { RequestMessage = request };
                    }
                }
                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", AccessToken);
            }
            var response = await base.SendAsync(request, cancellationToken).ConfigureAwait(false);
            if (!tokenRequired || response.StatusCode != HttpStatusCode.Unauthorized)
            {
                return response;
            }
            if (await RefreshTokensAsync(cancellationToken) == false)
            {
                return response;
            }
            response.Dispose(); // This 401 response will not be used for anything so is disposed to unblock the socket.
            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", AccessToken);
            return await base.SendAsync(request, cancellationToken).ConfigureAwait(false);
        }
        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Net.Http.DelegatingHandler" />, and optionally disposes of the managed resources.
        /// </summary>
        /// <param name="disposing">true to release both managed and unmanaged resources; false to releases only unmanaged resources.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && !_disposed)
            {
                _disposed = true;
                _lock.Dispose();
            }
            base.Dispose(disposing);
        }
        private async Task<bool> RefreshTokensAsync(CancellationToken cancellationToken)
        {
            var refreshToken = RefreshToken;
            if (String.IsNullOrEmpty(refreshToken))
            {
                return false;
            }
            if (await _lock.WaitAsync(Timeout, cancellationToken).ConfigureAwait(false))
            {
                try
                {
                    var response = await _oidcClient.RefreshTokenAsync(refreshToken).ConfigureAwait(false);
                    if (!response.IsError)
                    {
                        _accessToken = response.AccessToken;
                        if (!String.IsNullOrEmpty(response.RefreshToken))
                        {
                            _refreshToken = response.RefreshToken;
                        }
                        _ = Task.Run(() =>
                        {
                            foreach (EventHandler<TokenRefreshedEventArgs> del in TokenRefreshed.GetInvocationList())
                            {
                                try
                                {
                                    del(this, new TokenRefreshedEventArgs(response.AccessToken, response.RefreshToken, IdentityToken));
                                }
                                catch { }
                            }
                        }).ConfigureAwait(false);
                        return true;
                    }
                }
                finally
                {
                    _lock.Release();
                }
            }
            return false;
        }
        private async Task<string> GetAccessTokenAsync(CancellationToken cancellationToken)
        {
            if (await _lock.WaitAsync(Timeout, cancellationToken).ConfigureAwait(false))
            {
                try
                {
                    return _accessToken;
                }
                finally
                {
                    _lock.Release();
                }
            }
            return null;
        }
    }
}