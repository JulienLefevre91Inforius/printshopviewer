﻿using System;
using System.Collections.Generic;

namespace PrintshopViewer.Domain.Dtos
{
    public enum EnumStatutValidationClient
    {
        Tous = 1,
        AValider = 2,
        Valides = 3,
        Refuses = 4
    }

    public class StatutValidationClient
    {
        public EnumStatutValidationClient Statut { get; set; }
        public string Libelle { get; set; }

        public StatutValidationClient(EnumStatutValidationClient enumStatutValidationClient, string libelle)
        {
            Statut = enumStatutValidationClient;
            Libelle = libelle;
        }
    }

    public class StatutValidationClientListe : List<StatutValidationClient>
    {
        public StatutValidationClientListe()
        {
            this.Clear();
            this.Add(new (EnumStatutValidationClient.Tous, "Tous"));
            this.Add(new (EnumStatutValidationClient.AValider, "A valider"));
            this.Add(new (EnumStatutValidationClient.Valides, "Validés"));
            this.Add(new (EnumStatutValidationClient.Refuses, "Refusés"));
        }
    }
}
