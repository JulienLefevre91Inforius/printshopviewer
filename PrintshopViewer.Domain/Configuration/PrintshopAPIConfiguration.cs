﻿
namespace PrintshopViewer.Domain.Configuration
{
    public class PrintshopAPIConfiguration
    {
        public string GetToken { get; set; }
        public string GetLotsClient { get; set; }
        public string PostValiderLots { get; set; }
        public string PostRefuserLots { get; set; }
        public string GetDownloadLot { get; set; }
        public int TimeoutRequest { get; set; }
    }
}
