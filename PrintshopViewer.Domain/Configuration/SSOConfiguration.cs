﻿
namespace PrintshopViewer.Domain.Configuration
{
    public class SSOConfiguration
    {
        public string Url { get; set; }
        public string ApiName { get; set; }
        public string Scope { get; set; }
        public string RedirectUri { get; set; }
        public string ClientSecret { get; set; }
    }
}