﻿using IdentityModel.OidcClient;
using Newtonsoft.Json;
using PrintshopViewer.BusinessLayer.Helpers;
using PrintshopViewer.Domain.Configuration;
using PrintshopViewer.Domain.Dtos;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace PrintshopViewer.BusinessLayer.Services
{
    public class PrintshopService : IPrintshopService
    {

        PrintshopAPIConfiguration _printshopAPIConfiguration;
        public PrintshopService(PrintshopAPIConfiguration printshopAPIConfiguration)
        {
            _printshopAPIConfiguration = printshopAPIConfiguration;
        }
        public async Task<Response<List<LotClientRetour>>> GetLotsClient(string accessToken, LotClient lotCLient)
        {
            try
            {
                using (HttpClient httpClient = new (new HttpClientHandler()))
                {
                    httpClient.Timeout = TimeSpan.FromSeconds(_printshopAPIConfiguration.TimeoutRequest);
                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
                    HttpContent httpContent = new StringContent(JsonConvert.SerializeObject(lotCLient));
                    httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                    HttpRequestMessage httpRequestMessage = new()
                    {
                        Method = HttpMethod.Get,
                        RequestUri = new Uri(_printshopAPIConfiguration.GetLotsClient),
                        Content = httpContent
                    };

                    HttpResponseMessage httpResponseMessage = await httpClient.SendAsync(httpRequestMessage);

                    if (httpResponseMessage.IsSuccessStatusCode)
                        return new Response<List<LotClientRetour>>(true, JsonConvert.DeserializeObject<List<LotClientRetour>>(httpResponseMessage.Content.ReadAsStringAsync().Result));
                    else
                        return new Response<List<LotClientRetour>>(false, null);
                }
            }
            catch (HttpRequestException)
            {
                return new Response<List<LotClientRetour>>(false, null);
            }
        }
        public async Task<Response<ValidationClientRetour>> ValiderLots(string accessToken, ValidationClient validationClient)
        {
            try
            {
                using (HttpClient httpClient = new(new HttpClientHandler()))
                {
                    httpClient.Timeout = TimeSpan.FromSeconds(_printshopAPIConfiguration.TimeoutRequest);
                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
                    HttpContent httpContent = new StringContent(JsonConvert.SerializeObject(validationClient));
                    httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    HttpResponseMessage httpResponseMessage = await httpClient.PostAsync(_printshopAPIConfiguration.PostValiderLots, httpContent);

                    if (httpResponseMessage.IsSuccessStatusCode)
                        return new Response<ValidationClientRetour>(true, JsonConvert.DeserializeObject<ValidationClientRetour>(httpResponseMessage.Content.ReadAsStringAsync().Result));
                    else
                        return new Response<ValidationClientRetour>(false, null);
                }
            }
            catch (HttpRequestException)
            {
                return new Response<ValidationClientRetour>(false, null);
            }
        }
        public async Task<Response<ValidationClientRetour>> RefuserLots(string accessToken, RefusClient validationClient)
        {
            try
            {
                using (HttpClient httpClient = new(new HttpClientHandler()))
                {
                    httpClient.Timeout = TimeSpan.FromSeconds(_printshopAPIConfiguration.TimeoutRequest);
                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
                    HttpContent httpContent = new StringContent(JsonConvert.SerializeObject(validationClient));
                    httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    HttpResponseMessage httpResponseMessage = await httpClient.PostAsync(_printshopAPIConfiguration.PostRefuserLots, httpContent);

                    if (httpResponseMessage.IsSuccessStatusCode)
                        return new Response<ValidationClientRetour>(true, JsonConvert.DeserializeObject<ValidationClientRetour>(httpResponseMessage.Content.ReadAsStringAsync().Result));
                    else
                        return new Response<ValidationClientRetour>(false, null);
                }
            }
            catch (HttpRequestException)
            {
                return new Response<ValidationClientRetour>(false, null);
            }
        }
        public async Task<Response<TelechargerLotRetour>> GetTelechargerLot(string accessToken, TelechargerLot telechargerLot)
        {
            try
            {
                using (HttpClient httpClient = new(new HttpClientHandler()))
                {
                    httpClient.Timeout = TimeSpan.FromSeconds(_printshopAPIConfiguration.TimeoutRequest);
                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
                    HttpContent httpContent = new StringContent(JsonConvert.SerializeObject(telechargerLot));
                    httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                    HttpRequestMessage httpRequestMessage = new()
                    {
                        Method = HttpMethod.Get,
                        RequestUri = new Uri(_printshopAPIConfiguration.GetDownloadLot),
                        Content = httpContent
                    };

                    HttpResponseMessage httpResponseMessage = await httpClient.SendAsync(httpRequestMessage);

                    if (httpResponseMessage.IsSuccessStatusCode)
                        return new Response<TelechargerLotRetour>(true, new TelechargerLotRetour() { File = await httpResponseMessage.Content.ReadAsByteArrayAsync() }) ;
                    else
                        return new Response<TelechargerLotRetour>(false, null);
                }
            }
            catch (HttpRequestException)
            {
                return new Response<TelechargerLotRetour>(false, null);
            }
        }
    }
}
