﻿using PrintshopViewer.Domain.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PrintshopViewer.BusinessLayer.Services
{
    public interface IPrintshopService
    {
        Task<Response<List<LotClientRetour>>> GetLotsClient(string accessToken, LotClient lotCLient);
        Task<Response<ValidationClientRetour>> ValiderLots(string accessToken, ValidationClient validationClient);
        Task<Response<ValidationClientRetour>> RefuserLots(string accessToken, RefusClient validationClient);
        Task<Response<TelechargerLotRetour>> GetTelechargerLot(string accessToken, TelechargerLot telechargerLot);
    }
}
