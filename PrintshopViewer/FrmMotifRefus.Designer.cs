﻿namespace PrintshopViewer
{
    partial class FrmMotifRefus
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.printshopServiceBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.btnValider = new System.Windows.Forms.Button();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.lblTitreInput = new System.Windows.Forms.Label();
            this.pnlButtons = new System.Windows.Forms.Panel();
            this.lblMessage = new System.Windows.Forms.Label();
            this.txtMotifRefus = new System.Windows.Forms.TextBox();
            this.btnRefuser = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.printshopServiceBindingSource)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            this.pnlButtons.SuspendLayout();
            this.SuspendLayout();
            // 
            // printshopServiceBindingSource
            // 
            this.printshopServiceBindingSource.DataSource = typeof(PrintshopViewer.BusinessLayer.Services.PrintshopService);
            // 
            // btnValider
            // 
            this.btnValider.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnValider.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnValider.Location = new System.Drawing.Point(124, 6);
            this.btnValider.Name = "btnValider";
            this.btnValider.Size = new System.Drawing.Size(87, 23);
            this.btnValider.TabIndex = 5;
            this.btnValider.Text = "Oui";
            this.btnValider.UseVisualStyleBackColor = true;
            this.btnValider.Click += new System.EventHandler(this.btnValider_Click);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.AutoSize = true;
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.lblTitreInput, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.pnlButtons, 0, 5);
            this.tableLayoutPanel2.Controls.Add(this.lblMessage, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.txtMotifRefus, 0, 4);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 6;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(316, 217);
            this.tableLayoutPanel2.TabIndex = 11;
            // 
            // lblTitreInput
            // 
            this.lblTitreInput.AutoSize = true;
            this.lblTitreInput.Location = new System.Drawing.Point(3, 55);
            this.lblTitreInput.Name = "lblTitreInput";
            this.lblTitreInput.Size = new System.Drawing.Size(71, 15);
            this.lblTitreInput.TabIndex = 9;
            this.lblTitreInput.Text = "lblTitreInput";
            // 
            // pnlButtons
            // 
            this.pnlButtons.BackColor = System.Drawing.SystemColors.Control;
            this.pnlButtons.Controls.Add(this.btnRefuser);
            this.pnlButtons.Controls.Add(this.btnValider);
            this.pnlButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlButtons.Location = new System.Drawing.Point(0, 184);
            this.pnlButtons.Margin = new System.Windows.Forms.Padding(0);
            this.pnlButtons.Name = "pnlButtons";
            this.pnlButtons.Size = new System.Drawing.Size(316, 35);
            this.pnlButtons.TabIndex = 8;
            // 
            // lblMessage
            // 
            this.lblMessage.AutoSize = true;
            this.lblMessage.Location = new System.Drawing.Point(3, 20);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(90, 15);
            this.lblMessage.TabIndex = 8;
            this.lblMessage.Text = "Message Golbal";
            // 
            // txtMotifRefus
            // 
            this.txtMotifRefus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtMotifRefus.Location = new System.Drawing.Point(3, 73);
            this.txtMotifRefus.Multiline = true;
            this.txtMotifRefus.Name = "txtMotifRefus";
            this.txtMotifRefus.Size = new System.Drawing.Size(310, 108);
            this.txtMotifRefus.TabIndex = 7;
            // 
            // btnRefuser
            // 
            this.btnRefuser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRefuser.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnRefuser.Location = new System.Drawing.Point(217, 6);
            this.btnRefuser.Name = "btnRefuser";
            this.btnRefuser.Size = new System.Drawing.Size(87, 23);
            this.btnRefuser.TabIndex = 6;
            this.btnRefuser.Text = "Non";
            this.btnRefuser.UseVisualStyleBackColor = true;
            this.btnRefuser.Click += new System.EventHandler(this.btnRefuser_Click);
            // 
            // FrmMotifRefus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(316, 217);
            this.Controls.Add(this.tableLayoutPanel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmMotifRefus";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Printshop Viewer";
            ((System.ComponentModel.ISupportInitialize)(this.printshopServiceBindingSource)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.pnlButtons.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.BindingSource printshopServiceBindingSource;
        private System.Windows.Forms.Button btnValider;
        private System.Windows.Forms.Label lblTitreInput;
        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.TextBox txtMotifRefus;
        private System.Windows.Forms.Panel pnlButtons;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button btnRefuser;
    }
}
