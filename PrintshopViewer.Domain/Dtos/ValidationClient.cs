﻿using System;
using System.Collections.Generic;

namespace PrintshopViewer.Domain.Dtos
{
    public class ValidationClient
    {
        public string CodeClient { get; set; }

        public List<ValidationClientFolioId> FoliosId { get; set; }
    }

    public class ValidationClientFolioId
    {
        public string FolioId { get; set; }
    }

    public class RefusClient
    {
        public string CodeClient { get; set; }
        public string MotifRefus { get; set; }

        public List<ValidationClientFolioId> FoliosId { get; set; }
    }

    public class ValidationClientRetour
    {
        public List<ValidationClientFolioId> FolioIdsSuccess { get; set; }

        public List<ValidationClientFolioId> FolioIdsError { get; set; }
    }

}
