﻿using IdentityModel.OidcClient;
using PrintshopViewer.BusinessLayer.Helpers;
using PrintshopViewer.BusinessLayer.Services;
using PrintshopViewer.Domain.Configuration;
using PrintshopViewer.Domain.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IdentityModel.Tokens.Jwt;
using System.IO;

namespace PrintshopViewer
{
    public partial class FrmAccueil : Form
    {
        #region Déclarations
        IPrintshopService _printshopService;
        SSOConfiguration _ssoConfiguration;
        private string _codeClient;
        private OidcClient _oidcClient;
        private OidcClientOptions _oidcOptions;
        private HttpClient _apiClient;
        private RefreshTokenHandler _refreshTokenHandler;
        private  string _messageBoxTitre = "Validation des bons à tirer";
        #endregion

        #region Constructeurs
        public FrmAccueil(IPrintshopService printshopService, SSOConfiguration ssoConfiguration)
        {
            _printshopService = printshopService;
            _ssoConfiguration = ssoConfiguration;
            InitializeComponent();
            pnlAccueil.Visible = false;
        }

        #endregion

        #region Evénements
        private async void Accueil_Load(object sender, EventArgs e)
        {
            if(await RecupererToken())
            {
                pnlAccueil.Visible = true;
                lblEtat.Text = String.Empty;
                ConstruireListeStatuts();
                cmbStatutValidationClient.SelectedValue = EnumStatutValidationClient.Tous;
                await ChargerLotsClients();
            }
            else
                Application.Exit();
        }
        private async void cmbStatutValidationClient_SelectionChangeCommitted(object sender, EventArgs e)
        {
            await ChargerLotsClients();
        }
        private async void btnValiderLots_Click(object sender, EventArgs e)
        {
            await ValiderLotsClient();
        }
        private async void btnRefuserLots_Click(object sender, EventArgs e)
        {
            await RefuserLotsClients();
        }
        private void dtgLotsClient_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            string folioId = ((DataGridView)sender).CurrentRow.Cells[nameof(LotClientRetour.FoliodId)].Value.ToString() ?? string.Empty;
            SaveFileDialog saveFileDialog = new();
            saveFileDialog.Filter = "PDF document (*.pdf)|*.pdf";
            saveFileDialog.FileName = $"{folioId}.pdf";
            if (saveFileDialog.ShowDialog() == DialogResult.OK && !string.IsNullOrEmpty(saveFileDialog.FileName))
            {
                if (!backgroundWorker.IsBusy)
                    this.backgroundWorker.RunWorkerAsync(new LotDocument() { FolioId = folioId, FilePathToSave = saveFileDialog.FileName });
            }
        }
        private async void backgroundWorker_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            await TelechargerLotClient((LotDocument)e.Argument);
        }
        #endregion

        #region Fonctionnalités
        public async Task<bool> RecupererToken()
        {
            _oidcOptions = new OidcClientOptions()
            {
                Authority = _ssoConfiguration.Url,
                ClientId = _ssoConfiguration.ApiName,
                Scope = _ssoConfiguration.Scope,
                RedirectUri = _ssoConfiguration.RedirectUri,
                Browser = new WinFormsWebView(),
                BrowserTimeout = TimeSpan.FromSeconds(1),
                ClientSecret = _ssoConfiguration.ClientSecret,
                IdentityTokenValidator = new NoValidationIdentityTokenValidator(),
            };

            _oidcClient = new OidcClient(_oidcOptions);
            LoginResult loginResult = await _oidcClient.LoginAsync().ConfigureAwait(false);

            if (loginResult.IsError)
                return false;
            else
            {
                //create Api HttpClient
                _refreshTokenHandler = new RefreshTokenHandler(_oidcClient, loginResult.AccessToken, loginResult.RefreshToken, loginResult.IdentityToken);
                _apiClient = new HttpClient(_refreshTokenHandler);
                _apiClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                JwtSecurityTokenHandler handler = new();
                JwtSecurityToken jwtSecurityToken = handler.ReadJwtToken(loginResult.AccessToken);
                if (jwtSecurityToken.Payload["client_pcid"] != null)
                    _codeClient = jwtSecurityToken.Payload["client_pcid"].ToString();
                else
                    return false;

                return true;
            }
        }
        private async Task ChargerLotsClients()
        {
            NotifierTacheEnCours("Récupération des lots...");

            Response<List<LotClientRetour>> responseLotClientListe = await _printshopService.GetLotsClient(_refreshTokenHandler.AccessToken, new LotClient() { CodeClient = _codeClient });

            if (!responseLotClientListe.Success)
                MessageBox.Show("Erreur lors de la récupération des bons à tirer");
            else
            {
                switch (cmbStatutValidationClient.SelectedValue)
                {
                    case EnumStatutValidationClient.Tous:
                        ConstruireTableauLotsClient(responseLotClientListe.Result);
                        break;
                    case EnumStatutValidationClient.AValider:
                        ConstruireTableauLotsClient(responseLotClientListe.Result.Where(x => !x.ValidationClient.HasValue).ToList());
                        break;
                    case EnumStatutValidationClient.Valides:
                        ConstruireTableauLotsClient(responseLotClientListe.Result.Where(x => x.ValidationClient.HasValue && x.ValidationClient.Value).ToList());
                        break;
                    case EnumStatutValidationClient.Refuses:
                        ConstruireTableauLotsClient(responseLotClientListe.Result.Where(x => x.ValidationClient.HasValue && !x.ValidationClient.Value).ToList());
                        break;
                }
            }
            FinTacheEnCours();
        }
        private void ConstruireListeStatuts()
        {
            cmbStatutValidationClient.DisplayMember = nameof(StatutValidationClient.Libelle);
            cmbStatutValidationClient.ValueMember = nameof(StatutValidationClient.Statut);
            cmbStatutValidationClient.DataSource = new StatutValidationClientListe();
        }
        private void ConstruireTableauLotsClient(List<LotClientRetour> lotsClientListe)
        {
            dtgLotsClient.DataSource = lotsClientListe;

            dtgLotsClient.Columns[nameof(LotClientRetour.LotId)].Visible = false;
            dtgLotsClient.Columns[nameof(LotClientRetour.ValidationClient)].Visible = false;
            dtgLotsClient.Columns[nameof(LotClientRetour.FoliodId)].HeaderText = "Folio Id";
            dtgLotsClient.Columns[nameof(LotClientRetour.DateCreation)].HeaderText = "Date de création";
            dtgLotsClient.Columns[nameof(LotClientRetour.NombreCourriers)].HeaderText = "Nombre de courriers";
            dtgLotsClient.Columns[nameof(LotClientRetour.DateDepot)].HeaderText = "Date de dépôt";
            dtgLotsClient.Columns[nameof(LotClientRetour.ValidationClient)].HeaderText = "Validé";
            dtgLotsClient.Columns[nameof(LotClientRetour.DateValidationClient)].HeaderText = "Date de validation";
            dtgLotsClient.Columns[nameof(LotClientRetour.MotifRefusClient)].HeaderText = "Motif de refus";
            dtgLotsClient.Columns[nameof(LotClientRetour.DateAbandon)].HeaderText = "Date d'abandon";
            dtgLotsClient.Columns[nameof(LotClientRetour.MotifAbandon)].HeaderText = "Motif d'abandon";
            dtgLotsClient.Columns[nameof(LotClientRetour.DateConfirmationImpression)].HeaderText = "Date de confirmation d'impression";
            dtgLotsClient.Columns[nameof(LotClientRetour.DateExpedition)].HeaderText = "Date d'expédition";

            if (dtgLotsClient.Columns[$"chk{nameof(LotClientRetour.ValidationClient)}"] == null)
            { 
                DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn = new();
                dataGridViewCheckBoxColumn.HeaderText = "Validé";
                dataGridViewCheckBoxColumn.ThreeState = true;
                dataGridViewCheckBoxColumn.Name = $"chk{nameof(LotClientRetour.ValidationClient)}";

                dtgLotsClient.Columns.Insert(dtgLotsClient.Columns[nameof(LotClientRetour.ValidationClient)].Index, dataGridViewCheckBoxColumn);
            }

            foreach (DataGridViewRow row in dtgLotsClient.Rows)
            {
                bool? valideParClient = (bool?)row.Cells[nameof(LotClientRetour.ValidationClient)].Value;
                if (valideParClient.HasValue && valideParClient.Value)
                    row.Cells[$"chk{nameof(LotClientRetour.ValidationClient)}"].Value = CheckState.Checked;
                else if (valideParClient.HasValue && !valideParClient.Value)
                        row.Cells[$"chk{nameof(LotClientRetour.ValidationClient)}"].Value = CheckState.Unchecked;
                else
                    row.Cells[$"chk{nameof(LotClientRetour.ValidationClient)}"].Value = CheckState.Indeterminate;
            }
        }
        private async Task ValiderLotsClient()
        {
            NotifierTacheEnCours("Validation des lots en cours...");

            if (dtgLotsClient.SelectedRows.Count == 0)
            {
                MessageBox.Show("Veuillez sélectionner au moins 1 bon à tirer.", _messageBoxTitre, MessageBoxButtons.OK);
                return;
            }

            List<string> aLotsAValider = new();
            List<string> aLotsNonValidables = new();
            foreach (DataGridViewRow aDataGridViewRow in dtgLotsClient.SelectedRows)
            {
                if (((bool?)aDataGridViewRow.Cells[nameof(LotClientRetour.ValidationClient)].Value).HasValue || ((DateTime?)aDataGridViewRow.Cells[nameof(LotClientRetour.DateAbandon)].Value).HasValue)
                    aLotsNonValidables.Add($"{aDataGridViewRow.Cells[nameof(LotClientRetour.FoliodId)].Value}");
                else
                    aLotsAValider.Add($"{aDataGridViewRow.Cells[nameof(LotClientRetour.FoliodId)].Value}");
            }

            if (aLotsNonValidables.Any())
                MessageBox.Show($"Les bons à tirer suivants ne sont plus à valider et ne peuvent pas être sélectionnés pour validation :\r\n\r\n{String.Join("\r\n", aLotsNonValidables)}", _messageBoxTitre, MessageBoxButtons.OK);
            else if (MessageBox.Show($"Vous êtes sur le point de valider le(s) bon(s) à tirer suivants, continuer ?\r\n\r\n{String.Join("\r\n", aLotsAValider)}",
                    _messageBoxTitre, MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Response<ValidationClientRetour> responseValidationClientListe = await _printshopService.ValiderLots(_refreshTokenHandler.AccessToken, new ValidationClient()
                {
                    CodeClient = _codeClient,
                    FoliosId = aLotsAValider.Select(x => new ValidationClientFolioId() { FolioId = x }).ToList()
                });

                if (!responseValidationClientListe.Success)
                    MessageBox.Show($"La validation des bons à tirer a échoué : \r\n\r\n", _messageBoxTitre, MessageBoxButtons.OK);
                else
                {
                    if (responseValidationClientListe.Result.FolioIdsError.Any())
                    {
                        MessageBox.Show($"Les bons à tirer suivants ont bien été validés :\r\n\r\n{String.Join("\r\n", responseValidationClientListe.Result.FolioIdsSuccess)}" +
                            $"Les bons à tirer suivants n'ont pas pu être validés :\r\n\r\n{String.Join("\r\n", responseValidationClientListe.Result.FolioIdsError)}", _messageBoxTitre, MessageBoxButtons.OK);
                        await ChargerLotsClients();
                    }
                    else
                    {
                        MessageBox.Show($"Les bons à tirer sélectionnés ont bien été validés.", _messageBoxTitre, MessageBoxButtons.OK);
                        await ChargerLotsClients();
                    }
                }
            }
            FinTacheEnCours();
        }
        private async Task RefuserLotsClients()
        {
            NotifierTacheEnCours("Refus des lots en cours...");

            string aMessageBoxTitre = "Refus de bons à tirer";
            if (dtgLotsClient.SelectedRows.Count == 0)
            {
                MessageBox.Show("Veuillez sélectionner au moins 1 bon à tirer.", aMessageBoxTitre, MessageBoxButtons.OK);
                return;
            }

            List<string> aLotsARefuser = new();
            List<string> aLotsNonValidables = new();
            foreach (DataGridViewRow aDataGridViewRow in dtgLotsClient.SelectedRows)
            {
                if (((bool?)aDataGridViewRow.Cells[nameof(LotClientRetour.ValidationClient)].Value).HasValue || ((DateTime?)aDataGridViewRow.Cells[nameof(LotClientRetour.DateAbandon)].Value).HasValue)
                    aLotsNonValidables.Add($"{aDataGridViewRow.Cells[nameof(LotClientRetour.FoliodId)].Value}");
                else
                    aLotsARefuser.Add($"{aDataGridViewRow.Cells[nameof(LotClientRetour.FoliodId)].Value}");
            }

            if (aLotsNonValidables.Any())
                MessageBox.Show($"Les bons à tirer suivants ne sont plus à valider et ne peuvent pas être sélectionnés pour refus :\r\n\r\n{String.Join("\r\n", aLotsNonValidables)}", aMessageBoxTitre, MessageBoxButtons.OK);
            else
            {
                FrmMotifRefus frmMotifRefus = new($"Vous êtes sur le point de refuser le(s) bon(s) à tirer suivants, continuer ?\r\n\r\n{String.Join("\r\n", aLotsARefuser)}", aMessageBoxTitre, "Motif du refus");
                frmMotifRefus.ShowDialog();

                if (frmMotifRefus.DialogResult == DialogResult.Yes)
                {
                    Response<ValidationClientRetour> responseValidationClientListe = await _printshopService.RefuserLots(_refreshTokenHandler.AccessToken, new RefusClient()
                    {
                        CodeClient = _codeClient,
                        MotifRefus = frmMotifRefus.MotifRefus,
                        FoliosId = aLotsARefuser.Select(x => new ValidationClientFolioId() { FolioId = x }).ToList()
                    });

                    if (!responseValidationClientListe.Success)
                        MessageBox.Show($"Le refus des bons à tirer a échoué : \r\n\r\n", aMessageBoxTitre, MessageBoxButtons.OK);
                    else
                    {
                        if (responseValidationClientListe.Result.FolioIdsError.Any())
                        {
                            MessageBox.Show($"Les bons à tirer suivants ont bien été refusés :\r\n\r\n{String.Join("\r\n", responseValidationClientListe.Result.FolioIdsSuccess)}" +
                                $"Les bons à tirer suivants n'ont pas pu être refusés :\r\n\r\n{String.Join("\r\n", responseValidationClientListe.Result.FolioIdsError)}", aMessageBoxTitre, MessageBoxButtons.OK);
                            await ChargerLotsClients();
                        }
                        else
                        {
                            MessageBox.Show($"Les bons à tirer sélectionnés ont bien été refusés.", aMessageBoxTitre, MessageBoxButtons.OK);
                            await ChargerLotsClients();
                        }
                    }
                }
            }
            FinTacheEnCours();
        }
        private async Task TelechargerLotClient(LotDocument lotDocument)
        {
            NotifierTacheEnCours("Téléchargement du bon à tirer en cours...");
            Response<TelechargerLotRetour> responseTelechargerLot = await _printshopService.GetTelechargerLot(_refreshTokenHandler.AccessToken, new TelechargerLot() { CodeClient = _codeClient, Lot = lotDocument.FolioId });
            if (!responseTelechargerLot.Success)
                MessageBox.Show($"Impossible de télécharger le bon à tirer : \r\n\r\n", _messageBoxTitre, MessageBoxButtons.OK);
            else
            {
                NotifierTacheEnCours("Sauvegarde du bon à tirer...");
                await System.IO.File.WriteAllBytesAsync(lotDocument.FilePathToSave, responseTelechargerLot.Result.File);
                System.Diagnostics.Process.Start(new System.Diagnostics.ProcessStartInfo(lotDocument.FilePathToSave) { UseShellExecute = true });             
            }
            FinTacheEnCours();
        }

        private void NotifierTacheEnCours(string message) 
        {
            lblEtat.Invoke(new Action<string>(ChangerTexteEtat), message);
            UseWaitCursor = true;
            Cursor.Current = Cursors.WaitCursor;
        }
        private void FinTacheEnCours()
        {
            lblEtat.Invoke(new Action<string>(ChangerTexteEtat), string.Empty);
            UseWaitCursor = false;
            Cursor.Current = Cursors.Arrow;
        }
        private void ChangerTexteEtat(string message)
        {
            lblEtat.Text = message;
        }
        #endregion       
    }
}
