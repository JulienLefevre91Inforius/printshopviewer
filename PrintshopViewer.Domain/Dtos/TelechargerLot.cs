﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintshopViewer.Domain.Dtos
{
    
    public class TelechargerLot
    {
        public string CodeClient { get; set; }
        public string Lot { get; set; }
    }

    public class TelechargerLotRetour
    {
        public byte[] File { get; set; }
    }

    public class LotDocument
    {
        public string FolioId { get; set; }
        public string FilePathToSave { get; set; }
    }
}
